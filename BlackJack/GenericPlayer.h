#pragma once
#include <string>
#include "Hand.h"

using namespace std;

class GenericPlayer : public Hand
{
public:
	GenericPlayer(const string& name = "");
	friend ostream& operator<<(ostream& os, const GenericPlayer& aGenericPlayer);
	virtual ~GenericPlayer();
	virtual bool isHitting() const = 0;
	bool IsBusted() const;
	void Bust() const;
protected:
	string m_Name;
};
