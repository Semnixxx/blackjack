#pragma once
#include "GenericPlayer.h"

using namespace std;

class Player : public GenericPlayer
{
public:
	Player(const string& name = "");
	virtual ~Player();
	virtual bool isHitting() const;
	void Win() const;
	void Lose() const;
	void Push() const;
};